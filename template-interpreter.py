#!/usr/bin/env python

import re
import argparse
import os
import PrimeCompliance
import pyprime
from pyprime import prime_config
from progress.bar import Bar


def do_prime_things(prime, destination, apply_fix=False):
    """
    Gets a list of templates from Prime within the hierarchy under My Templates/RETS Engineering,
    excluding the ones in the Development folder
    :param prime: Instance of the PrimeInstance class
    :param destination: Destination folder the policy files will be stored in
    :param apply_fix:
    :return: 
    """
    template_list = prime.find_prime_objects(prime_config.templates,
                                             {'path': ['startsWith("My Templates/RETS Engineering")',
                                                       'notStartsWith("My Templates/RETS Engineering/Development")']})
    file_list = []
    basic_templates = []
    variable_templates = []
    complex_templates = []
    skipped_files = []
    composite_templates = []
    print('\n')
    bar = Bar('Processing templates', max=len(template_list))
    for template_id in template_list:
        # First, update the progress bar
        bar.next()
        # for each template found, read its contents.
        template = prime.prime_request(prime_config.templates + template_id['$'], None).json()
        filename = template['queryResponse']['entity'][0]['cliTemplateDTO']['name'] + '.xml'
        file_list.append(filename)

        if debug_flag:
            print(template['queryResponse']['entity'][0]['cliTemplateDTO']['name'])
        data_search = template['queryResponse']['entity'][0]['cliTemplateDTO']['content']
        # data_search validates that this template is a valid CLI
        # template with CLI commands section and returns just that section.
        if data_search:
            result = process_template_text(data_search, filename, destination, apply_fix)
            if result == "basic_template":
                basic_templates.append(filename)
            elif result == "variable_template":
                variable_templates.append(filename)
            elif result == "complex_template":
                complex_templates.append(filename)
            else:
                skipped_files.append(filename)
    bar.finish()
    print('\n\nPolicies have been generated for the following templates:')
    for file_n in basic_templates:
        print(file_n)
    for file_n in variable_templates:
        print(file_n)

    print('\n\nComplex templates - these templates can\'t be automatically converted to compliance policies:')
    for file_n in complex_templates:
        print(file_n)
    if len(complex_templates) == 0:
        print("**None**")

    print('\n\nSkipped Files - These files should be investigated:')
    for file_n in skipped_files:
        print(file_n)
    if len(skipped_files) == 0:
        print("**None**")

    print('\n\nComposite Templates - These files can be ignored:')
    for file_n in composite_templates:
        print(file_n)
    if len(composite_templates) == 0:
        print("**None**")


def create_xml_output(config_lines, filename, apply_fix=False):
    """
    Iterates over the provided config and builds the compliance policy XML for Prime.
    :param config_lines:
    :param filename:
    :param apply_fix: Bool
    :return: str - XML String
    """
    whitespace_regex = re.compile('^[\s*!]*$')  # Matches empty lines and lines with only whitespace
    grouping_regex = re.compile(r'(##(?:Rule|Section)##.*?|^.*?)(?=\n##(?:Rule|Section)##|$)', re.DOTALL)  # Identifies and groups the config.
    header_regex = re.compile(r'##Rule## ?(.*?)\n', re.DOTALL)  # Matches just the rule header
    section_regex = re.compile(r'##Section## ?(.*?)\n', re.DOTALL)  # Matches just the section header
    policy = PrimeCompliance.PrimeCompliancePolicy(filename[0:-4].replace('-', '_'))
    regexed_config = grouping_regex.findall(config_lines)
    for group in regexed_config:
        # Loop through those groups.
        matched_header = header_regex.match(group)    # Matches the heading for this rule
        section_header = section_regex.match(group)  # Matches the heading for this section
        raw_config = remove_template_comments(group)  # Matches the config excluding heading and comments
        if matched_header:
            rule = create_rule_from_config(raw_config, matched_header.group(1), apply_fix, False)
            policy.add_rule(rule)
        elif section_header:
            rule = create_rule_from_config(raw_config, section_header.group(1), apply_fix, True)
            policy.add_rule(rule)
        else:
            for line in raw_config.splitlines():
                if not whitespace_regex.match(line):
                    rule = create_rule_from_config(line, None, apply_fix)
                    policy.add_rule(rule)
    return policy.return_policy_xml()


def create_rule_from_config(config_lines, name=None, apply_fix=False, apply_as_section=False):
    """
    Create a rule from standard config lines. Returns an object of class PrimeCompliance.PrimeComplianceRule
    :param config_lines:
    :param name:
    :param apply_fix:
    :param apply_as_section:
    :return: PrimeCompliance.PrimeComplianceRule
    """
    whitespace_regex = re.compile('^[\s*!]*$')  # Matches empty lines and lines with only whitespace
    if name is None:
        if debug_flag:
            print(config_lines)
        name = config_lines
        name = name.strip()
    rule = PrimeCompliance.PrimeComplianceRule(name)
    config_by_line = config_lines.splitlines()
    for n in range(0, len(config_by_line)):
        line = config_by_line[n].strip()
        if not whitespace_regex.match(line):
            if apply_as_section:
                if n == 0:
                    fix_text = config_lines
                    section = None
                else:
                    fix_text = config_by_line[0] + "\n" + config_by_line[n]
                    section = config_by_line[0]
            else:
                fix_text = None
                section = None
            rule.add_condition(config_by_line[n], True, apply_fix, fix_text, section)
            if debug_flag:
                print('|' + config_by_line[n] + '|')
    return rule


def extract_text_re(body: str, regex: str, re_flags=re.DOTALL):
    """
    Performs a regular expression search on 'body' using 'regex' as the Regular Expression to match.
    :param body:
    :param regex:
    :param re_flags:
    :return: re.search_result
    """
    p = re.compile(regex, re_flags)
    return p.search(body)


def remove_template_comments(text: str) -> str:
    """
    Searches the provided text for various comment types and removes them from the config text
    :param text:
    :return:
    """

    # Remove Prime Template comments of type ##
    replace_regex = re.compile('##.*$', re.MULTILINE)
    text = replace_regex.sub("", text)
    # Remove Prime Template comments of type #* ... *#
    replace_regex_2 = re.compile('#\*.*?\*#', re.DOTALL)
    text = replace_regex_2.sub("", text)
    # Remove Prime Template statement #set
    replace_regex_3 = re.compile(r'#set.*', re.IGNORECASE)
    text = replace_regex_3.sub("", text)
    # Remove IOS comment starting with '!'
    replace_regex_4 = re.compile(r'!.*', re.IGNORECASE)
    text = replace_regex_4.sub("", text)
    return text.lstrip()


def process_template_text(text: str, filename: str, destination: str, apply_fix=False) -> str:
    """
    This function takes the raw config text from the Prime templates, checks whether it is simple 
    enough to be converted to a policy and performs the conversion via create_xml_output()
    :param text: CLICommands from the template in question
    :param filename: Filename to be written to disk
    :param destination: Path to the destination folder for files written to disk
    :param apply_fix:
    :return:
    """
    template_conditionals = extract_text_re(text, '#if', re.DOTALL | re.IGNORECASE)
    template_forloops = extract_text_re(text, '#foreach', re.DOTALL | re.IGNORECASE)
    template_interactive = extract_text_re(text, '(#interactive|\<MLTCMD\>)', re.DOTALL | re.IGNORECASE)
    template_variables = extract_text_re(text, '\$[a-zA-Z][a-zA-Z0-9\-_]', re.DOTALL | re.IGNORECASE)
    new_filename = filename[0:-4] + '.txt'

    # Create folders if they don't exist:
    if not os.path.exists(destination):
        os.mkdir(destination)
    # if not os.path.exists(os.path.join(destination, 'templateswithvars')):
    #     os.mkdir(os.path.join(destination, 'templateswithvars'))
    if debug_flag and not os.path.exists(os.path.join(destination, 'basicconfigs')):
        os.mkdir(os.path.join(destination, 'basicconfigs'))
    if not os.path.exists(os.path.join(destination, 'policies')):
        os.mkdir(os.path.join(destination, 'policies'))

    if not template_conditionals and not template_forloops and not template_interactive:
        if template_variables:
            result = "variable_template"
        else:
            result = "basic_template"
        if debug_flag:
            with open(os.path.join(destination, 'basicconfigs', new_filename), 'w') as basic_file:
                basic_file.write(text)
        with open(os.path.join(destination, 'policies', filename), 'w') as policy_output:
            policy_output.write(create_xml_output(text, filename, apply_fix))
    else:
        result = "complex_template"
    if debug_flag:
        with open(os.path.join(destination, new_filename), 'w') as output:
            output.write(text)
    return result


def do_file_things(filepath: str, destination: str, apply_fix: bool = False):
    """
    This function will collect all the template files from the specified folder, assess whether they are candidates for
    converting to a policy and if so, generate the policy via the process_template_text function
    :param filepath: str
    :param destination: str
    :param apply_fix: bool
    :return:
    """
    file_list = []
    skipped_files = []
    composite_templates = []
    basic_templates = []
    variable_templates = []
    complex_templates = []
    # Search the filepath given for all .xml files.
    for dirpath, dirnames, filenames in os.walk(filepath):
        for filename in [f for f in filenames if f.endswith('.xml')]:
            file_list.append(os.path.join(dirpath, filename))
            # for each file found, read its contents and evaluate as a template.
            if debug_flag:
                print(filename)
            with open(os.path.join(dirpath, filename), 'r') as file:
                file_content = file.read()
            data_search = extract_text_re(file_content, '<ootb-template>.*<clicommand>(.*)</clicommand>.*')
            # data_search validates that this template is a valid CLI
            # template with CLI commands section and returns just that section.
            if data_search:
                result = process_template_text(data_search.group(1), filename, destination, apply_fix)
                if result == "basic_template":
                    basic_templates.append(filename)
                elif result == "variable_template":
                    variable_templates.append(filename)
                elif result == "complex_template":
                    complex_templates.append(filename)
                else:
                    skipped_files.append(filename)
            else:
                # If we don't find normal template data, check if its a composite template.
                composite_search = extract_text_re(file_content,
                                                   '.*<com\.cisco\.ifm\.template\.importExport\.ExportContainer>')
                if composite_search:
                    # Its a composite template, add to the list and move on
                    composite_templates.append(filename)
                else:
                    # Its not a composite template, add to the list of skipped templates for investigation.
                    skipped_files.append(filename)

    print('\n\nPolicies have been generated for the following templates:')
    for file_n in basic_templates:
        print(file_n)
    for file_n in variable_templates:
        print(file_n)

    print('\n\nComplex templates - these templates can\'t be automatically converted to compliance policies:')
    for file_n in complex_templates:
        print(file_n)
    if len(complex_templates) == 0:
        print("**None**")

    print('\n\nSkipped Files - These files should be investigated:')
    for file_n in skipped_files:
        print(file_n)
    if len(skipped_files) == 0:
        print("**None**")

    print('\n\nComposite Templates - These files can be ignored:')
    for file_n in composite_templates:
        print(file_n)
    if len(composite_templates) == 0:
        print("**None**")


if __name__ == '__main__':
    # This is the main program that gets run if this script is called from the shell.
    parser = argparse.ArgumentParser(description='Process Prime template exports and create\
     compliance policies from them.')
    # First we set up command line arguments and parse them
    arg_group = parser.add_mutually_exclusive_group(required=True)
    arg_group.add_argument('-p', '--from-prime', action='store', metavar='username',
                           help="Collect config templates directly from Prime Infrastructure")
    arg_group.add_argument('-f', '--from-template-folder', action='store', metavar="folder",
                           help='Path to the folder containing template files.')
    arg_group.add_argument('-t', '--from-text-file', action='store', metavar="config_file", type=argparse.FileType('r'),
                           help='Generate compliance policy from single config text file')
    parser.add_argument('-c', '--prime-host', action='store', metavar="hostname",
                        help='Specify the hostname for the Cisco PI server', default=prime_config.prime_host)
    parser.add_argument('-i', '--prime-ignore-ssl', action='store_true', help='Disable SSL Verification')
    parser.add_argument('-d', '--destination-path', action='store', metavar="dest",
                        help='Folder to save templates and policies', default=os.path.join('.', 'prime_templates'))
    parser.add_argument('-r', '--apply-fix', action='store_true',
                        help='Include commands to fix the config in the conditions - USE WITH CAUTION')
    parser.add_argument('--debug', action='store_true', help='Print debug messages while working')

    args = parser.parse_args()
    # Set some constants from the parsed arguments
    debug_flag = args.debug
    verify_ssl = not args.prime_ignore_ssl

    if args.from_prime is not None:
        args.from_template_folder = 'Prime Host ' + args.prime_host
    if debug_flag:
        print('Source path selected is ' + args.from_template_folder)
        print('Destination path selected is ' + args.destination_path)

    # Check whether configurations will be imported from Prime, from a folder or a single file.
    if args.from_prime is not None:
        # Import from Prime
        prime_instance = pyprime.PrimeInstance(args.prime_host, args.from_prime, None, verify_ssl)
        do_prime_things(prime_instance, args.destination_path, args.apply_fix)
    elif args.from_template_folder is not None:
        # Import exported templates from a folder
        do_file_things(args.from_template_folder, args.destination_path, args.apply_fix)
    else:
        # Import config from a single file. Note this is not an exported
        # template file, but a normal text file containing only IOS commands
        out_filename = os.path.splitext(args.from_text_file.name)[0] + '.xml'
        config_text = args.from_text_file.read()
        args.from_text_file.close()
        template_result = process_template_text(config_text, out_filename, args.destination_path, args.apply_fix)
        if (template_result == 'basic_template') or (template_result == 'variable_template'):
            print('Created policy file in \'' +
                  os.path.abspath(os.path.join(args.destination_path, 'policies', out_filename)) + '\'')
        elif template_result == 'complex_template':
            print('Template is too complex to generate policy from. Remove #if, #foreach and <mltcmd>\
             statements and try again')
        else:
            print('Template was not understood. This probably means a syntax error somewhere in the file.')
