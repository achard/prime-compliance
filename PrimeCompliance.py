def create_identifier(line):
    import re
    line = re.sub('[/.*#<>^+!:{"};$]', '', line)
    line = re.sub('[- ]', '_', line)
    return line


class PrimeComplianceRuleCondition:
    def __init__(self, config_text: str, match_text: bool=True, apply_fix: bool=False, fix_text=None, section=None):
        """

        :param config_text: str
        :param match_text: bool
        :param apply_fix: bool
        :param fix_text: str
        :param section: str
        """
        import re
        self.section = section
        self.fixable = apply_fix
        self.config_text = re.escape(config_text)
        self.original_config_text = config_text
        variable_regex = re.compile(r'\\\$[a-zA-Z][a-zA-Z0-9\-_]*')
        self.config_text = variable_regex.sub('.*', self.config_text)
        if fix_text is not None:
            self.fix_text = fix_text
        else:
            self.fix_text = self.original_config_text
        if self.config_text != re.escape(config_text):
            self.fixable = False
        if match_text:
            self.title = "matches " + self.config_text
            self.match_action = 'CONTINUE'
            self.non_match_action = 'RAISE_AND_CONTINUE'
            self.violation_message = self.original_config_text + ' not found'
            self.action_xml = """            <MatchAction>CONTINUE</MatchAction>
            <NonMatchAction>RAISE_AND_CONTINUE</NonMatchAction>"""
        else:
            self.title = "does not match " + self.config_text
            self.match_action = 'RAISE_AND_CONTINUE'
            self.non_match_action = 'CONTINUE'
            self.violation_message = self.original_config_text + ' found'
            self.action_xml = """            <MatchAction>CONTINUE</MatchAction>
            <NonMatchAction>RAISE_AND_CONTINUE</NonMatchAction>"""
        if section is not None:
            self.scope = '''             <SubmodeRegExp><![CDATA[(''' + section + ''')]]></SubmodeRegExp>
            <Scope>SUBMODE_CONFIG</Scope>'''
        else:
            self.scope = '''            <Scope>ALL_CONFIG</Scope>'''

    def fetch_action_xml(self):
        return self.action_xml

    def fetch_display_string(self):
        return "Configuration must contain the string " + self.config_text

    def fetch_config_text(self):
        return self.config_text

    def __str__(self):
        if self.fixable:
            output_string = '''          <Condition>
            <Type>RAW_CONFIG</Type>
            <DisplayString><![CDATA[''' + self.fetch_display_string() + ''']]></DisplayString>
''' + self.action_xml + '''
            <BlockParams>
              <PassCriterion>ALL</PassCriterion>
              <RaiseViolationForAllInstances>true</RaiseViolationForAllInstances>
            </BlockParams>
            <RegexpParams>
              <PassCriterion>ALL</PassCriterion>
              <RaiseViolationForAllInstances>true</RaiseViolationForAllInstances>
            </RegexpParams>
            <Severity>3</Severity>
            <ViolationMessage><![CDATA[''' + self.original_config_text + ''' is missing]]></ViolationMessage>
            <Violation>
              <Message><![CDATA[''' + self.original_config_text + ''' is missing]]></Message>
              <Fix><![CDATA[''' + self.fix_text + ''']]></Fix>
            </Violation>
''' + self.scope + '''
            <Operator>MATCHES_EXPRESSION</Operator>
            <Value><![CDATA[(''' + self.config_text + ''')]]></Value>
          </Condition>'''
        else:
            output_string = '''          <Condition>
            <Type>RAW_CONFIG</Type>
            <DisplayString><![CDATA[''' + self.fetch_display_string() + ''']]></DisplayString>
''' + self.action_xml + '''
            <BlockParams>
              <PassCriterion>ALL</PassCriterion>
              <RaiseViolationForAllInstances>true</RaiseViolationForAllInstances>
            </BlockParams>
            <RegexpParams>
              <PassCriterion>ALL</PassCriterion>
              <RaiseViolationForAllInstances>true</RaiseViolationForAllInstances>
            </RegexpParams>
            <Severity>3</Severity>
''' + self.scope + '''
            <Operator>MATCHES_EXPRESSION</Operator>
            <Value><![CDATA[(''' + self.config_text + ''')]]></Value>
          </Condition>'''
        return output_string

    return_condition_xml = __str__


class PrimeComplianceRule:
    def __init__(self, rule_name: str, match_text: bool=True):
        """

        :param rule_name:
        :param match_text:
        """
        self.conditions = []
        self.title = rule_name
        self.match_text = match_text

    def add_condition(self, config, match_text: bool=None, apply_fix: bool=False, fix_prefix=None, section=None):
        if match_text is None:
            match_text = self.match_text
        self.conditions.append(PrimeComplianceRuleCondition(config, match_text, apply_fix, fix_prefix, section))

    def add_formed_condition(self, condition: PrimeComplianceRuleCondition):
        self.conditions.append(condition)

    def fetch_conditions(self):
        return self.conditions

    def fetch_title(self):
        return self.title

    def fetch_match_text(self):
        return self.match_text

    def identifier(self) -> str:
        identifier = create_identifier(self.title)
        if len(self.title) > 50:
            identifier = identifier[0:50]
        return '_'+identifier

    def __str__(self):
        conditions = ""
        for condition in self.conditions:
            conditions = conditions + condition.return_condition_xml() + "\n"
        return'''      <Rule identifier="''' + self.identifier() + '''">
        <Title><![CDATA[''' + self.title + ''']]></Title>
        <Impact><![CDATA[None]]></Impact>
        <VersionSelectorRefs>
          <VersionSelectorRef>cisco</VersionSelectorRef>
        </VersionSelectorRefs>
         <RuleType>RAW_CONFIG</RuleType>
        <Conditions>
''' + conditions + '''        </Conditions>
      </Rule>'''

    return_rule_xml = __str__


class PrimeCompliancePolicy:
    def __init__(self, title: str, regex=False):
        """

        :param title:
        """
        self.title = title
        self.rules = []
        self.regex = regex

    def add_rule_match(self, config: str):
        self.rules.append(PrimeComplianceRule(config, True))

    def add_rule(self, rule: PrimeComplianceRule):
        self.rules.append(rule)

    def fetch_rules(self):
        return self.rules

    def fetch_title(self):
        return self.title

    def identifier(self) -> str:
        identifier = create_identifier(self.title)

        if len(self.title) > 50:
            identifier = identifier[0:50]
        return '_'+identifier

    def __str__(self):
        rules = ""
        for rule in self.rules:
            rules = rules + rule.return_rule_xml() + "\n"
        return '''<CustomPolicy xmlns="http://www.cisco.com/nccm/api/schemas/1.1" id="''' + self.identifier() + '''">
    <Title><![CDATA[''' + self.title + ''']]></Title>
    <Categories>
       <Category id="_CCM_USER_DEFINED" type="category">CCM_USER_DEFINED</Category>
    </Categories>
    <VersionSelectorRefs>
      <VersionSelectorRef><![CDATA[cisco]]></VersionSelectorRef>
    </VersionSelectorRefs>
    <References>
      <Reference identifier="Custom">
        <Source>Custom</Source>
        <Sections>Custom Sections</Sections>
        <Versions>Custom Versions</Versions>
      </Reference>
    </References>
    <Rules>
''' + rules + '''    </Rules>
</CustomPolicy>'''
    return_policy_xml = __str__
